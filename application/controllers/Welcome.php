<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent:: __construct();
        $this->load->model('Users_model');
	}
	
	public function index()
	{
		$this->load->helper('url')->view('welcome_message');
	}

	public function signup()
	{
		$this->load->helper('url')->view('signup');
	}

	public function Dasboard()
	{
		$this->load->helper('url')->view('Dasbord');
	}
	public function proses_login() { 
        $email = $this->input->post(‘emailUser’); 
		$pass = $this->input->post(‘passwordUser’); 
		$data = array(
			'email' => $email,
			'password' => $pass,
		);

        $login = $this->Users_model->cek_user($data); 

        if (!empty($login)) { 
            // login berhasil 
            $this->session->set_userdata($login); 
            redirect(base_url()."dasboard"); 
        } else { 
            // login gagal 
            echo "<script>alert('Login gagal')</script>"; 
            redirect(base_url(‘welcome’)); 
        } 
    }

}