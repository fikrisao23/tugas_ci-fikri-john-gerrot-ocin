<!DOCTYPE html>
<!--Import materialize.css-->
<html>

<head>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/materialize.min.css"
        media="screen,projection" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/materialize.min.js"></script>
	<script src="//cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
        var table;
 
            $(document).ready(function() {
            
                //datatables
                table = $('#table').DataTable({ 
            
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.
            
                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo site_url('kelolaUser/ajax_list')?>",
                        "type": "POST"
                    },
            
                    //Set column definition initialisation properties.
                    "columnDefs": [
                    { 
                        "targets": [ 0 ], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                    ],
            
                });
            
            });
	</script>
    <title>Kelola User</title>
    <style type="text/css">
    .productsans {
        font-family: 'productsans', 'roboto', sans-serif;
    }

    @font-face {
        font-family: 'productsans';
        src: url(fonts/ProductSansRegular.ttf);
        font-weight: 400;
    }

    @font-face {
        font-family: 'productsans';
        src: url(fonts/ProductSansBold.ttf);
        font-weight: bold;
    }
    </style>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body class="productsans green">
    <ul id="slide-out" class="side-nav fixed">
        <li>
			<div class="userView">
				<p class="green-text lighten 1" style="font-weight: 300;font-size: 30px">Dashboard.</p>
			</div>
        </li>
        <li><a href="<?php echo base_url(); ?>kelolaUser" style="letter-spacing:1.5px;"><i
                    class="material-icons">supervisor_account</i>Kelola user</a></li>
        <li><a href="<?php echo base_url(); ?>welcome" style="letter-spacing:1.5px;"><i
                    class="material-icons">power_settings_new</i>Log Out</a></li <li style="margin-top:30px;">
        <div class="divider"></div>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
    <div class="row" style="margin-left:280px;">
        <div class="container" style="width:90%;">
            <h1 class="white-text align-center" style="font-size: 50px;margin-top: 0px;line-height: 110px">Kelola
                User<br>
            </h1>
            <div class="divider" style="height:3px;"></div>
            <div class="card">
                <div class="card-content">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs tabs-fixed-width">
                                <li class="tab col s3"><a class="active" href="#test1">Kelola User</a></li>
                                <li class="tab col s3"><a href="#test2">Input User</a></li>
                            </ul>
                        </div>
                        <div id="test1" class="col s12">
                        <table id="table" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
 
        </table>
                    </div>
                    
                        <div id="test2" class="col s12">
                            <form method="post" action="<?php echo base_url('kelolaUser/add') ?>" multipart="form/data">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input placeholder="Username" name="usernameUser" type="text" class="validate"
                                            required>
                                    </div>
                                    <div class="input-field col s6">
                                        <input placeholder="Email" name="emailUser" type="email" class="validate"
                                            required>
                                    </div>
                                    <div class="input-field col s6">
                                        <input placeholder="Password" type="password" name="passwordUser"
                                            class="validate" required>
                                    </div>
                                    <div class="input-field col s6">
                                        <select name="roleUser" required>
                                            <option value="Admin">Admin</option>
                                            <option value="Pemasok Lagu">Pemasok Lagu</option>
                                            <option value="Pembuat Playlist">Pembuat Playlist</option>
                                        </select>
                                    </div>
                                    <div id="bungkusbutton" class="center">
                                        <div class="col s12">
                                            <button type="submit" class="btn btn-large green"
                                                style="border-radius:30px;"><i
                                                    class="material-icons left">send</i>Submit</button>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Import jQuery before materialize.js-->

</body>

</html>
