<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/materialize.min.css"
        media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
<div class="container">
	<div class="card green lighten 3"
		 style="width:45%;margin-left:auto;margin-right:auto;display:block;margin-top:10%;border-radius: 20px">
		<div class="card-content">
			<form method="post" action="<?= base_url().'welcome/proses_login'; ?>" multipart="form/data">
				<div class="row">
					<div class="col s12">
						<p style="font-size: 20px">Sign In to your <span class="white-text" style="font-size: 20px">Account.</span> </p>
					</div>
					<div class="input-field col s12">
						<input placeholder="Email" type="email" name="emailUser" class="validate" required
							   style="border:1px solid #ddd;border-radius:10px;padding-left:5%;width:95%;background-color: white">
					</div>
					<div class="input-field col s12">
						<input placeholder="Password" type="password" name="passwordUser" class="validate" required
							   style="border:1px solid #ddd;border-radius:10px;padding-left:5%;width:95%;background-color: white">
					</div>
					<div class="input-field col s12">
						<div class="g-recaptcha" data-sitekey="6LcyuasUAAAAAGcQ_4H0q9hLJF4Hn6Ow6IWTUIwX"></div>
					</div>
					<div class="center">
						<div class="col md s12">
							<button type="submit" class="btn btn-large white green-text lighten 1" style="border-radius: 10px">Log In</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/materialize.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>

</html>
