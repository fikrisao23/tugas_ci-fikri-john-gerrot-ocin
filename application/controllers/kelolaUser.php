<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kelolaUser extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
        parent:: __construct();
        $this->load->model('Users_model','userz');
        $this->load->library('form_validation');
	}
	
	public function index()
	{
        $data["users"] = $this->userz->getAll();
		$this->load->helper('url')->view('User',$data);
	}

    public function add()
    {
        $user = $this->userz;
        $user->save();
        print "<script type='text/javascript'>alert('Akun berhasil di buat!');</script>";
        redirect(site_url('kelolaUser'));
    }
    public function edit($id = null)
    {
        if (!isset($id)) redirect('kelolaUser');
       
        $user = $this->userz;
        $user->update($id);
        print "<script type='text/javascript'>alert('Akun berhasil di edit!');</script>";
        redirect(site_url('kelolaUser'));
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->userz->delete($id)) {
            redirect(site_url('kelolaUser'));
        }
    }
	public function ajax_list()
    {
        $list = $this->userz->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->name;
            $row[] = $customers->email;
            $row[] = $customers->role;
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->userz->count_all(),
                        "recordsFiltered" => $this->userz->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

}