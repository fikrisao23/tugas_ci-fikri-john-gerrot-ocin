<!DOCTYPE html>
<!--Import materialize.css-->
<html>

<head>
	<!--Import materialize.css-->
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/materialize.min.css" media="screen,projection" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Dashboard</title>
    <style type="text/css">

        .productsans {
            font-family: 'productsans', 'roboto', sans-serif;
        }

        @font-face {
            font-family: 'productsans';
            src: url(fonts/ProductSansRegular.ttf);
            font-weight: 400;
        }

        @font-face {
            font-family: 'productsans';
            src: url(fonts/ProductSansBold.ttf);
            font-weight: bold;
        }
    </style>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body class="productsans green">
    <ul id="slide-out" class="side-nav fixed">
        <li>
            <div class="userView">
         	<p class="green-text lighten 1" style="font-weight: 300;font-size: 30px">Dashboard.</p>
            </div>
        </li>
        <li><a href="<?php echo base_url(); ?>kelolaUser" style="letter-spacing:1.5px;"><i class="material-icons">supervisor_account</i>Kelola user</a></li>
        <li><a href="<?php echo base_url(); ?>welcome" onclick="return confirm('Apakah anda yakin ingin log out?')" style="letter-spacing:1.5px;"><i class="material-icons">power_settings_new</i>Log Out</a></li
        <li style="margin-top:30px;">
            <div class="divider"></div>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
    <div class="row" style="margin-left:280px;">
        <div class="container" style="width:90%;">
            <h1 class="white-text align-center" style="font-size: 50px;margin-top: 0px;line-height: 110px">Welcome,<br>
                <span style="font-weight:bold;font-size: 80px;">John,Fikri,Ocin,Gerrot</span></h1>
            <div class="divider" style="height:3px;"></div>
        </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/materialize.min.js"></script>
    <script type="text/javascript">

    </script>

</body>

</html>
