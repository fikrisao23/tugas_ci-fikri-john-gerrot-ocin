<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>css/materialize.min.css"
        media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body class="blue">
    <div class="container">
        <div class="card"
            style="border-radius:60px;width:45%;margin-left:auto;margin-right:auto;display:block;margin-top:10%;">
            <div class="card-content">
                <div class="center">
                    <img src="<?php echo base_url(); ?>img/logo.svg" class="responsive-img" style="height:70px;" alt="">
                </div>
                <form method="post" multipart="form/data">
                    <div class="row">
                        <div class="input-field col s12">
                            <input placeholder="Name" type="text" class="validate" required
                                style="border:1px solid #ddd;border-radius:30px;padding-left:5%;width:95%;">
                        </div>
                        <div class="input-field col s12">
                            <input placeholder="Email" type="email" class="validate" required
                                style="border:1px solid #ddd;border-radius:30px;padding-left:5%;width:95%;">
                        </div>
                        <div class="input-field col s12">
                            <input placeholder="Password" type="password" class="validate" required
                                style="border:1px solid #ddd;border-radius:30px;padding-left:5%;width:95%;">
                        </div>
                        <div class="input-field col s12">
                            <input placeholder="Confirm Password" type="password" class="validate" required
                                style="border:1px solid #ddd;border-radius:30px;padding-left:5%;width:95%;">
                        </div>
                        <div id="bungkusbtn">
                            <div id="tmblsubmit" class="col md s6">
                                <button type="submit" class="btn btn-large blue" name="login_btn"
                                    style="border-radius:30px;"><i class="material-icons left">send</i>Submit</button>
                            </div>
                            <div id="tmblback" class="col md s6">
                                <div class="right">
                                    <a href="<?= base_url(); ?>welcome" class="btn btn-large blue"
                                        style="border-radius:30px;"><i class="material-icons left">send</i>Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/materialize.min.js"></script>
</body>

</html>