<?php
date_default_timezone_set('Asia/Jakarta');
class Users_model extends CI_Model {
  
  public $id;
  public $username;
  public $email;
  public $password;
  public $role;
  public $created_at;
  var $_table = 'tbl_user';
  var $table = 'tbl_user';
  var $column_order = array(null, 'name','email','role'); //set column field database for datatable orderable
  var $column_search = array('name','email','role'); //set column field database for datatable searchable 
  var $order = array('id' => 'asc'); // default order
  public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    public function cek_user($data) {
        // $this->db->where('email' , $email);
        // $this->db->where('password', md5($password));
        // $query = $this->db->get(‘tbl_user’);
        // return $query->row_array();
        $sql = "SELECT * FROM tbl_user WHERE email = ? AND password = ?";
        $hasil = $this->db->query($sql,$data);
        return $hasil;
    }

    public function getAll(){
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["product_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        // $this->id = null;
        // $this->name = $post["usernameUser"];
        // $this->email = $post["emailUser"];
        // $this->password = md5($post["passwordUser"]);
        // $this->role = $post["roleUser"];
        // $this->created_at = date("Y-m-d h:i:s");
        $timestamp = date("Y-m-d h:i:s");
        $sql = "INSERT INTO `tbl_user`(`id`, `name`, `email`, `password`, `role`, `created_at`) VALUES (NULL,'$post[usernameUser]','$post[emailUser]','$post[passwordUser]','$post[roleUser]','$timestamp')";
        // $this->db->insert($this->_table, $this);
        $this->db->query($sql);
    }

    public function update($id)
    {
        $post = $this->input->post();
        $timestamp = date("Y-m-d h:i:s");
        if(empty($post['passwordUser'])) {
            $sql = "UPDATE `tbl_user` SET `name`='$post[usernameUser]',`email`='$post[emailUser]',`role`='$post[roleUser]' WHERE `id` = $id";
        }else{
            $sql = "UPDATE `tbl_user` SET `name`='$post[usernameUser]',`email`='$post[emailUser]',`password`='$post[passwordUser]',`role`='$post[roleUser]' WHERE `id` = $id";    
        }
        // $this->db->insert($this->_table, $this);
        $this->db->query($sql);
        // $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }
    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}